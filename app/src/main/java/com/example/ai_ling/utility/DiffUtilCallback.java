package com.example.ai_ling.utility;

import androidx.recyclerview.widget.DiffUtil;

import com.example.ai_ling.ui.Chat.MessageObject;

import java.util.List;

//public class DiffUtilCallback extends DiffUtil.Callback {
//
//    private final List<MessageObject> oldList;
//    private final List<MessageObject> newList;
//
//    public DiffUtilCallback(List<MessageObject> oldList, List<MessageObject> newList) {
//        this.oldList = oldList;
//        this.newList = newList;
//    }
//
//
//    @Override
//    public int getOldListSize() {
//        return oldList.size();
//    }
//
//    @Override
//    public int getNewListSize() {
//        return newList.size();
//    }
//
//    @Override
//    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
//        MessageObject oldProduct = oldList.get(oldItemPosition);
//        MessageObject newProduct = newList.get(newItemPosition);
//        return oldProduct.getDate() == newProduct.getDate();
//    }
//
//    @Override
//    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
//        MessageObject oldProduct = oldList.get(oldItemPosition);
//        MessageObject newProduct = newList.get(newItemPosition);
//        return oldProduct.getText().equals(newProduct.getText())
//                && oldProduct.getFromUserKey().equals(newProduct.getFromUserKey());
//    }
//}
