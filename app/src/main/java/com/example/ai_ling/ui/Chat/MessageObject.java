package com.example.ai_ling.ui.Chat;

import java.util.Date;

public class MessageObject {
    private String text;
    private Date date;
    private String fromUserKey;

    public MessageObject(){

    }


    public MessageObject(String text, String fromUserKey) {
        this.text = text;
        this.date = new Date();
        this.fromUserKey = fromUserKey;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFromUserKey() {
        return fromUserKey;
    }

    public void setFromUserKey(String fromUserKey) {
        this.fromUserKey = fromUserKey;
    }

}
