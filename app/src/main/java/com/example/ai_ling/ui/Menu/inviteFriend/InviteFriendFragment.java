package com.example.ai_ling.ui.Menu.inviteFriend;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ai_ling.R;

public class InviteFriendFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inviteFriend();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_friends, container, false);
        Button btnInviteFriend = view.findViewById(R.id.btn_invite_friend);
        btnInviteFriend.setOnClickListener(view1 -> inviteFriend());
        return view;
    }

    private void inviteFriend() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        //TODO: вставить ссылку на сайт
        intent.putExtra(Intent.EXTRA_TEXT, "Давай изучать английский вместе: *ссылка на сайт*");
        startActivity(Intent.createChooser(intent, "Поделиться"));
    }
}




