package com.example.ai_ling.ui.Chat;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai_ling.R;
import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<MessageObject> messageList;


    public MessageAdapter(Context context, List<MessageObject> messageList){
        this.inflater = LayoutInflater.from(context);
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_message, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MessageObject object = messageList.get(position);
        if (object.getFromUserKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            holder.outgoItem.setVisibility(View.VISIBLE);
            holder.incomeItem.setVisibility(View.GONE);
            holder.messageTextOutgo.setText(object.getText());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            holder.messageTimeOutgo.setText(sdf.format(object.getDate()));
        } else {
            holder.outgoItem.setVisibility(View.GONE);
            holder.incomeItem.setVisibility(View.VISIBLE);
            holder.messageTextIncome.setText(object.getText());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            holder.messageTimeIncome.setText(sdf.format(object.getDate()));
        }

        Collections.sort(messageList, new Comparator<MessageObject>() {
            @Override
            public int compare(MessageObject messageObject, MessageObject t1) {
                return messageObject.getDate().compareTo(t1.getDate());
            }
        });
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ConstraintLayout outgoItem;
        final TextView messageTextOutgo;
        final TextView messageTimeOutgo;

        final ConstraintLayout incomeItem;
        final TextView messageTextIncome;
        final TextView messageTimeIncome;

        ViewHolder(View itemView) {
            super(itemView);
            outgoItem = itemView.findViewById(R.id.outgo_item);
            messageTextOutgo = itemView.findViewById(R.id.message_text_outgo);
            messageTimeOutgo = itemView.findViewById(R.id.message_time_outgo);

            incomeItem = itemView.findViewById(R.id.income_item);
            messageTextIncome = itemView.findViewById(R.id.message_text_income);
            messageTimeIncome = itemView.findViewById(R.id.message_time_income);
        }

    }

}
