package com.example.ai_ling.ui.Search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Chat.MessageActivity;
import com.example.ai_ling.ui.Login.UserObject;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final List<UserObject> users;

    interface OnClickListener{
        void onStateClick(UserObject user, int position);
    }
    private final OnClickListener onClickListener;

    public SearchAdapter(Context context, List<UserObject> users, OnClickListener onClickListener) {
        this.inflater = LayoutInflater.from(context);
        this.users = users;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserObject user = users.get(position);
        holder.nameView.setText(user.getName());

        try {
        String photoUser = user.getPhotoUrl();
        Picasso.get()
                .load(Uri.parse(photoUser))
                .placeholder(R.drawable.ic_avatar)
                .into(holder.photoView);
        }catch (NullPointerException ignored){ }
        holder.itemView.setOnClickListener(view ->
                onClickListener.onStateClick(user, position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView nameView;
        CircleImageView photoView;

        ViewHolder(View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.user_name);
            photoView = itemView.findViewById(R.id.avatar_search);
        }

    }
}
