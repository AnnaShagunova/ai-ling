package com.example.ai_ling.ui.UserProfile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Chat.MessageActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class InterlocutorProfileActivity extends AppCompatActivity {

    ImageButton btnBack;
    CircleImageView photo;
    TextView name;
    TextView about;

    FirebaseAuth auth;
    String key;
    DatabaseReference ref;
    FirebaseDatabase database;

    Bundle arguments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_interlocutor);

        btnBack = findViewById(R.id.btn_back);
        photo = findViewById(R.id.interlocutor_photo);
        name = findViewById(R.id.interlocutor_name);
        about = findViewById(R.id.interlocutor_about);

        auth = FirebaseAuth.getInstance();

        arguments = getIntent().getExtras();
        key = arguments.get("key").toString();

        database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
        ref = database.getReference("Users");

        btnBack.setOnClickListener(view -> goingBack());

        getInfoInterlocutor();
    }

    private void getInfoInterlocutor() {
        ref.child(key).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String nameUser = snapshot.getValue().toString();
                name.setText(nameUser);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });

        ref.child(key).child("about").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                    String aboutUser = snapshot.getValue().toString();
                    about.setText(aboutUser);
                }catch (NullPointerException e){
                    ConstraintLayout aboutUser = findViewById(R.id.interlocutor_profile_about);
                    aboutUser.setVisibility(View.GONE);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });

        ref.child(key).child("photoUrl").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                String photoUser = snapshot.getValue().toString();
                Picasso.get()
                        .load(Uri.parse(photoUser))
                        .placeholder(R.drawable.ic_avatar)
                        .into(photo);
                }catch (NullPointerException ignored){ }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    private void goingBack() {
        finish();
    }
}
