package com.example.ai_ling.ui.Menu.home;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai_ling.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private List<HomeObject> mainList;

    interface OnClickListener{
        void onStateClick(HomeObject object, int position);
    }
    private final OnClickListener onClickListener;

    public HomeAdapter(Context context, List<HomeObject> mainList, OnClickListener onClickListener) {
        this.inflater = LayoutInflater.from(context);
        this.mainList = mainList;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HomeObject homeObject = mainList.get(position);
        holder.text.setText(homeObject.getText());
        holder.name.setText(homeObject.getName());
        try {
        String photoUser = homeObject.getPhotoUrl();
        Picasso.get()
                .load(Uri.parse(photoUser))
                .placeholder(R.drawable.ic_avatar)
                .into(holder.photoUrl);
        }catch (NullPointerException ignored){ }

        Date dataMessage = homeObject.getData();
        SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfDay = new SimpleDateFormat("dd MMM.");

        Calendar calEnd = Calendar.getInstance();
        calEnd.set(Calendar.HOUR, 23);
        calEnd.set(Calendar.MINUTE, 59);
        calEnd.set(Calendar.SECOND, 59);
        Date dataEnd = calEnd.getTime();

        Calendar calStart = Calendar.getInstance();
        calStart.set(Calendar.HOUR, 00);
        calStart.set(Calendar.MINUTE, 00);
        calStart.set(Calendar.SECOND, 00);
        Date dataStart = calStart.getTime();

        if(dataMessage.after(dataStart) && dataMessage.before(dataEnd)){
            holder.time.setText(sdfHour.format(dataMessage));
        } else {
            holder.time.setText(sdfDay.format(dataMessage));
        }

        holder.itemView.setOnClickListener(view -> onClickListener.onStateClick(homeObject, position));
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        final TextView name;
        final TextView text;
        final CircleImageView photoUrl;
        final TextView time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_home);
            text = itemView.findViewById(R.id.message_text_home);
            photoUrl = itemView.findViewById(R.id.avatar_home);
            time = itemView.findViewById(R.id.time_home);
        }
    }
}
