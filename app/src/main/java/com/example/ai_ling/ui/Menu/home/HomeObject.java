package com.example.ai_ling.ui.Menu.home;

import android.net.Uri;

import com.google.firebase.database.ServerValue;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HomeObject {

    private String text;
    private String name;
    private String photoUrl;
    private String idDialog;
    private Date data;

    public HomeObject(){}

    public HomeObject(String idDialog, String name, String text){
        this.name = name;
        this.text = text;
        this.idDialog = idDialog;
        this.data = new Date();
    }

    public HomeObject(String idDialog, String name, String text, String photoUrl) {
        this.name = name;
        this.text = text;
        this.photoUrl = photoUrl;
        this.idDialog = idDialog;
        this.data = new Date();
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getIdDialog() {
        return idDialog;
    }

    public void setIdDialog(String id) {
        this.idDialog = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
