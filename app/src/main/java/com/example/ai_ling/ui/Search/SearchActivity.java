package com.example.ai_ling.ui.Search;

import static android.view.KeyEvent.KEYCODE_ENTER;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Chat.MessageActivity;
import com.example.ai_ling.ui.Login.UserObject;
import com.example.ai_ling.ui.Menu.MainActivity;;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SearchActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;
    ImageButton btnBack;
    EditText searchUser;

    List<UserObject> usersList;
    UserObject userObject;
    String userId;

    RecyclerView recyclerSearch;
    RecyclerView.LayoutManager layoutManager;
    SearchAdapter adapter;

    FirebaseDatabase database;
    DatabaseReference ref;

    SearchAdapter.OnClickListener clickListener;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        relativeLayout =findViewById(R.id.search_activity);

        database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
        ref = database.getReference("Users");

        btnBack = findViewById(R.id.btn_back);
        searchUser = findViewById(R.id.search_user);

        intent = new Intent(this, MessageActivity.class);
        clickListener = ((user, position) -> startActivity(intent));

        usersList = new ArrayList<>();

        recyclerSearch = findViewById(R.id.recycler_search);
        layoutManager = new LinearLayoutManager(this);
        recyclerSearch.setLayoutManager(layoutManager);
        adapter = new SearchAdapter(this, usersList, clickListener);
        recyclerSearch.setAdapter(adapter);

        btnBack.setOnClickListener(view -> showMainActivity());
        searchUser.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KEYCODE_ENTER) {
                userId = searchUser.getText().toString();
                showFoundUsers(userId);
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                if (usersList!= null) usersList.clear();
                return true;
            }
            return false;
        });

    }

    private void showFoundUsers(String userId) {

        ref.orderByChild("userId").equalTo(userId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                userObject = snapshot.getValue(UserObject.class);
                saveUser();

                String foundUserId = snapshot.getKey();
                intent.putExtra("key", foundUserId);
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) { }
            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });

        ref.orderByChild("name").equalTo(userId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                userObject = snapshot.getValue(UserObject.class);
                saveUser();

                String foundUserId = snapshot.getKey();
                intent.putExtra("key", foundUserId);
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) { }
            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    private void showMainActivity() {
        startActivity(new Intent(SearchActivity.this, MainActivity.class));
    }

    private void saveUser(){
        if (usersList!= null) usersList.clear();
        usersList.add(userObject);
        adapter.notifyDataSetChanged();
    }
}
