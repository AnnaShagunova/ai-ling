package com.example.ai_ling.ui.Menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Search.SearchActivity;
import com.example.ai_ling.ui.UserProfile.ProfileActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ai_ling.databinding.ActivityMainBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;
    DatabaseReference ref;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_invite_friend, R.id.nav_setup, R.id.nav_feedback)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        NavigationView navigationUI = findViewById(R.id.nav_view);
        navigationUI.setItemIconTintList(null);

        // Переход в профиль
        View headerView = navigationView.getHeaderView(0);
        CircleImageView profile = (CircleImageView) headerView.findViewById(R.id.btnProfile);
        TextView name = (TextView) headerView.findViewById(R.id.name_menu);
        TextView email = (TextView) headerView.findViewById(R.id.email_menu);

        profile.setOnClickListener(view -> showProfile());


        key = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ref = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/").getReference("Users");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            name.setText(user.getDisplayName());
            email.setText(user.getEmail());

            ref.child(key).child("photoUrl").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    try {
                        String photoUser = snapshot.getValue().toString();
                        Picasso.get()
                                .load(Uri.parse(photoUser))
                                .placeholder(R.drawable.ic_avatar)
                                .into(profile);
                    }catch (NullPointerException ignored){ }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) { }
            });
        }

        ImageButton btnSearch = findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(view -> showSearch());
    }

    private void showSearch() {
        startActivity(new Intent(MainActivity.this, SearchActivity.class));
    }

    private void showProfile() {
        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
    }



    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

}