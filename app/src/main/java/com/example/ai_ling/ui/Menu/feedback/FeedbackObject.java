package com.example.ai_ling.ui.Menu.feedback;

public class FeedbackObject {
    private String id;
    private String name;
    private String email;
    private String feedbackBody;
    private boolean needAnswer;


    public FeedbackObject(String id, String name, String email, String feedbackBody, boolean needAnswer) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.feedbackBody = feedbackBody;
        this.needAnswer = needAnswer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFeedbackBody() {
        return feedbackBody;
    }

    public void setFeedbackBody(String feedbackBody) {
        this.feedbackBody = feedbackBody;
    }

    public boolean isNeedAnswer() {
        return needAnswer;
    }

    public void setNeedAnswer(boolean needAnswer) {
        this.needAnswer = needAnswer;
    }
}
