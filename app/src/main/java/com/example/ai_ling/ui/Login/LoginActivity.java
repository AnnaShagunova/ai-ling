package com.example.ai_ling.ui.Login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Menu.MainActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends AppCompatActivity {

    Button btnRegister;
    Button btnLogin;

    FirebaseAuth auth; // Авторизация
    FirebaseDatabase database; // Подключение к БД
    DatabaseReference ref; // Работа с данными внутри БД
    RelativeLayout relativeLayout;
    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnRegister = findViewById(R.id.registration_enter_view);
        btnLogin = findViewById(R.id.login_enter_view);

        relativeLayout = findViewById(R.id.login_activity);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
        ref = database.getReference("Users");


        firebaseUser = auth.getCurrentUser();
        if(firebaseUser != null){
            reload();
        }

        btnRegister.setOnClickListener(view -> showRegisterWindow());

        btnLogin.setOnClickListener(view -> showLoginWindow());
    }

    private void reload() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    private void showLoginWindow() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View loginWindow = inflater.inflate(R.layout.window_login, null);
        dialog.setView(loginWindow);

        MaterialEditText email = loginWindow.findViewById(R.id.email_login);
        MaterialEditText password = loginWindow.findViewById(R.id.password_login);

        dialog.setPositiveButton("Войти", (dialogInterface, i) -> {
            if (TextUtils.isEmpty(email.getText().toString())) {
                Snackbar.make(relativeLayout, "Неверно указана почта", Snackbar.LENGTH_SHORT).show();
                return;
            }

            if (password.getText().toString().length() < 5) {
                Snackbar.make(relativeLayout, "Неверно указан пароль", Snackbar.LENGTH_SHORT).show();
                return;
            }

            // Авторизация
            auth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnSuccessListener(authResult -> {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    })
                    .addOnFailureListener(e -> Snackbar.make(relativeLayout, "Ошибка авторизации: пользователь не найден", Snackbar.LENGTH_SHORT).show());
        }).setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.dismiss());

        dialog.show();
    }

    private void showRegisterWindow() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View registerWindow = inflater.inflate(R.layout.window_registr, null);
        dialog.setView(registerWindow);

        MaterialEditText name = registerWindow.findViewById(R.id.name_register);
        MaterialEditText email = registerWindow.findViewById(R.id.email_register);
        MaterialEditText password = registerWindow.findViewById(R.id.password_register);

        dialog.setPositiveButton("Готово", (dialogInterface, i) -> {
            if(TextUtils.isEmpty(email.getText().toString())) {
                Snackbar.make(relativeLayout, "Введите вашу почту", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if(TextUtils.isEmpty(name.getText().toString())) {
                Snackbar.make(relativeLayout, "Введите ваше имя", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if(password.getText().toString().length() < 6) {
                Snackbar.make(relativeLayout, "Пароль дожен быть больше 5 символов", Snackbar.LENGTH_SHORT).show();
                return;
            }

            //создание уникального id
            DatabaseReference postsRef = ref.child("posts");
            DatabaseReference pushedPostRef = postsRef.push();
            String postId = pushedPostRef.getKey();

            //Регистрация пользователя
            auth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnSuccessListener(authResult -> {
                    UserObject user = new UserObject();
                    user.setName(name.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPassword(password.getText().toString());
                    user.setUserId(postId);

                    FirebaseUser firebaseUser = auth.getCurrentUser();
                    UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder().setDisplayName(name.getText().toString()).build();
                    firebaseUser.updateProfile(profileUpdate);
                    finish();


                    ref.child(auth.getCurrentUser().getUid())
                          .setValue(user)
                          .addOnCompleteListener(task -> {
                              if (task.isSuccessful()) {
                                  Toast.makeText(LoginActivity.this, "Вы успешно зарегистрированы!", Toast.LENGTH_LONG).show();
                                  startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                  finish();

                              } else {
                                  Toast.makeText(LoginActivity.this, "Ошибка регистрации: недопустимая форма E-mail", Toast.LENGTH_LONG).show();

                              }
                          });
                  });
        });

        dialog.show();
    }

}
