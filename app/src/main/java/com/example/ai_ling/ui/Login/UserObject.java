package com.example.ai_ling.ui.Login;


public class UserObject {
    private String name;
    private String email;
    private String password;
    private String photoUrl;
    private String userId;

    public UserObject() {}

    public UserObject(String name, String email, String password, String photoUrl, String userId) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.photoUrl = photoUrl;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
