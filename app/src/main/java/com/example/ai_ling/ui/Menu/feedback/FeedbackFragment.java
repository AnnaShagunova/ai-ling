package com.example.ai_ling.ui.Menu.feedback;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Login.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FeedbackFragment extends Fragment {

    EditText name;
    EditText email;
    EditText body;
    CheckBox checkBox;
    Button btnSend;
    Spinner spinner;

    FirebaseDatabase database;
    DatabaseReference refFeedback;
    String userId;
    String feedbackId;
    FeedbackObject object;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feedback, container,false);
        name = view.findViewById(R.id.edit_text_name);
        email = view.findViewById(R.id.edit_text_email);
        body = view.findViewById(R.id.edit_text_feedback_body);
        checkBox = view.findViewById(R.id.check_box_response);
        btnSend = view.findViewById(R.id.btn_send_feedback);

        spinner = view.findViewById(R.id.spinner_feedback_type);
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.feedbacktypelist, R.layout.spinner_color);
        spinner.setAdapter(arrayAdapter);

        btnSend.setOnClickListener(view1 -> sendFeedback());

        return view;
    }


    private void sendFeedback() {
        getBodyEmail();
        if (name.length() != 0 && email.length() != 0 && body.length() != 0) {
            database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
            refFeedback = database.getReference("Feedback");
            feedbackId = refFeedback.push().getKey();

            refFeedback.child(getSubjectEmail()).child(feedbackId).setValue(object);

            name.setText("");
            email.setText("");
            body.setText("");
            Toast.makeText(getActivity(), "Отзыв успешно отправлен!", Toast.LENGTH_LONG).show();
        }else {Toast.makeText(getActivity(), "Нельзя отправить пустую форму", Toast.LENGTH_LONG).show();}
    }

    private String getSubjectEmail() {
        return spinner.getSelectedItem().toString();
    }

    private void getBodyEmail() {
        String feedbackName = name.getText().toString();
        String feedbackEmail = email.getText().toString();
        String feedbackBody = body.getText().toString();
        boolean feedbackBox = checkBox.isChecked();
        userId = FirebaseAuth.getInstance().getUid();

        object = new FeedbackObject(userId, feedbackName, feedbackEmail, feedbackBody, feedbackBox);
    }

}