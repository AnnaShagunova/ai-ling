package com.example.ai_ling.ui.Chat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Menu.MainActivity;
import com.example.ai_ling.ui.Menu.home.HomeObject;
import com.example.ai_ling.ui.UserProfile.InterlocutorProfileActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MessageActivity extends AppCompatActivity {
    EditText writeMessage;
    ImageButton sendMessage;

    ImageButton btnBack;
    CircleImageView btnProfileUser;
    TextView name;

    RecyclerView recyclerMessages;
    RecyclerView.LayoutManager layoutManager;
    MessageAdapter adapter;
    //DiffUtilCallback diffUtilResult;
    boolean isScrolling = false;
    int countMessages = 10;
    boolean smoothScrollToPosition = true;

    MessageObject messageObject;
    String userMessage;
    String messageId;
    String nameInterlocutor;
    HomeObject homeObjectUser;
    HomeObject homeObjectInterlocutor;

    List<MessageObject> messageList;

    String interlocutorKey;
    String userKey;
    DatabaseReference refUser;
    DatabaseReference refChats;
    DatabaseReference refMainList;
    FirebaseDatabase database;
    FirebaseAuth auth;

    Bundle arguments;
    Intent intent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        writeMessage = findViewById(R.id.write_message);
        sendMessage = findViewById(R.id.send_message);

        btnBack = findViewById(R.id.btn_back);
        btnProfileUser = findViewById(R.id.interlocutor_profile);

        name = findViewById(R.id.interlocutor_name_chat);

        intent = new Intent(this, InterlocutorProfileActivity.class);

        database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
        auth = FirebaseAuth.getInstance();

        refUser = database.getReference("Users");
        refChats = database.getReference("Chats");
        refMainList = database.getReference("MainScreen");

        userKey = auth.getCurrentUser().getUid();

        btnBack.setOnClickListener(view -> showMainActivity());
        btnProfileUser.setOnClickListener(view -> showProfileUser());

        getInterlocutorKey();

        messageList = new ArrayList<>();
        recyclerMessages = findViewById(R.id.recycler_messages);
        layoutManager = new LinearLayoutManager(this);
        recyclerMessages.setLayoutManager(layoutManager);
        adapter = new MessageAdapter(this, messageList);
        recyclerMessages.setAdapter(adapter);

//        diffUtilResult = new DiffUtilCallback(adapter.getList(), messageList);
//        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffUtilResult);
//        adapter.setList(messageList);
//        diffResult.dispatchUpdatesTo(adapter);

        sendMessage.setOnClickListener(view -> sendMessages());
        getMessage();
        //onScrollListener();
        getInfoInterlocutor();
    }

    private void getInterlocutorKey() {
        arguments = getIntent().getExtras();
        try {
            interlocutorKey = arguments.get("keyFromHome").toString();
        }catch (NullPointerException e){
            interlocutorKey = arguments.get("key").toString();
        }
        intent.putExtra("key", interlocutorKey);
    }

    private void sendMessages() {
        smoothScrollToPosition = true;
        userMessage = writeMessage.getText().toString();
        if(userMessage.length() != 0){
            messageObject = new MessageObject(userMessage, userKey);

            writeMessage.setText("");
            saveMessage();

            saveToMainScreen();
        }
    }

    private void saveToMainScreen() {
        String nameUser = auth.getCurrentUser().getDisplayName();
        DatabaseReference postsRef = refMainList.child("posts");
        DatabaseReference pushedPostRef = postsRef.push();
        String idDialog = pushedPostRef.getKey();

        refUser.child(userKey).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                    String photoUser = snapshot.getValue().toString();
                    homeObjectInterlocutor = new HomeObject(idDialog, nameUser, userMessage, photoUser);
                    refMainList.child(interlocutorKey).child(userKey).setValue(homeObjectInterlocutor);
                }catch (NullPointerException e){
                    homeObjectInterlocutor = new HomeObject(idDialog, nameUser, userMessage);
                    refMainList.child(interlocutorKey).child(userKey).setValue(homeObjectInterlocutor);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });

        refUser.child(interlocutorKey).child("photoUrl").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                    String photoInterlocutor = snapshot.getValue().toString();
                    homeObjectUser = new HomeObject(idDialog, nameInterlocutor, userMessage, photoInterlocutor);
                    refMainList.child(userKey).child(interlocutorKey).setValue(homeObjectUser);
                }catch (NullPointerException e){
                    homeObjectUser = new HomeObject(idDialog, nameInterlocutor, userMessage);
                    refMainList.child(userKey).child(interlocutorKey).setValue(homeObjectUser);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    private void saveMessage() {
        messageId = refChats.push().getKey();
        refChats.child(userKey).child(interlocutorKey).child(messageId).setValue(messageObject);
        refChats.child(interlocutorKey).child(userKey).child(messageId).setValue(messageObject);
    }

    private void getMessage() {
        messageList.clear();
        refChats.child(userKey).child(interlocutorKey).addChildEventListener(new ChildEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                MessageObject object = snapshot.getValue(MessageObject.class);
                if(!messageList.contains(object)){
                    messageList.add(object);
                    adapter.notifyDataSetChanged();
                }
                if(smoothScrollToPosition){
                    recyclerMessages.smoothScrollToPosition(adapter.getItemCount());
                }
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) { }
            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) { }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

//    private void onScrollListener() {
//        recyclerMessages.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
//                    isScrolling = true;
//                }
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if(isScrolling && dy < 0){
//                    updateScrolling();
//                }
//            }
//        });
//    }

//    private void updateScrolling() {
//        smoothScrollToPosition = false;
//        isScrolling = false;
//        countMessages += 10;
//        getMessage();
//    }


    private void getInfoInterlocutor() {
        refUser.child(interlocutorKey).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                nameInterlocutor = snapshot.getValue().toString();
                name.setText(nameInterlocutor);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });

        refUser.child(interlocutorKey).child("photoUrl").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                String photoUser = snapshot.getValue().toString();
                Picasso.get()
                        .load(Uri.parse(photoUser))
                        .placeholder(R.drawable.ic_avatar)
                        .into(btnProfileUser);
                }catch (NullPointerException ignored){ }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    private void showProfileUser() {
        startActivity(intent);
    }

    private void showMainActivity() {
        startActivity(new Intent(MessageActivity.this, MainActivity.class));
    }
}
