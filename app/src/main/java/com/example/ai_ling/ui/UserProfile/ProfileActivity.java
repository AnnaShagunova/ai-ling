package com.example.ai_ling.ui.UserProfile;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ai_ling.R;
import com.example.ai_ling.ui.Login.LoginActivity;
import com.example.ai_ling.ui.Menu.MainActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    FloatingActionButton addPhoto;
    ImageButton btnBack;
    AppCompatButton btnLogOut;

    CircleImageView photoProfile;
    TextView name;
    TextView email;
    TextView id;
    TextView about;

    ConstraintLayout btnProfileName;
    ConstraintLayout btnProfileAbout;
    ConstraintLayout btnProfileId;

    FirebaseAuth auth;
    DatabaseReference ref;
    FirebaseDatabase database;
    RelativeLayout relativeLayout;
    FirebaseUser fUser;
    UserProfileChangeRequest profileUpdate;

    FirebaseStorage storage;
    StorageReference storageRef;
    StorageReference mountainImagesRef;
    Uri resultUri;
    String key;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        addPhoto = findViewById(R.id.add_photo);
        btnBack = findViewById(R.id.btn_back);
        btnLogOut = findViewById(R.id.btn_log_out);

        btnProfileName = findViewById(R.id.interlocutor_name);
        btnProfileAbout = findViewById(R.id.interlocutor_profile_about);
        btnProfileId = findViewById(R.id.btn_profile_id);

        relativeLayout = findViewById(R.id.user_activity);

        photoProfile = findViewById(R.id.btnProfile);
        name = findViewById(R.id.interlocutor_profile_name);
        email = findViewById(R.id.profile_email);
        id = findViewById(R.id.profile_id);
        about = (TextView) findViewById(R.id.interlocutor_about);

        auth = FirebaseAuth.getInstance();
        key = auth.getCurrentUser().getUid();
        database = FirebaseDatabase.getInstance("https://ai-ling-default-rtdb.asia-southeast1.firebasedatabase.app/");
        ref = database.getReference("Users");
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

        addPhoto.setOnClickListener(view -> changePhotoUser());
        btnBack.setOnClickListener(view -> showMainActivity());
        btnLogOut.setOnClickListener(view -> userLogOut());

        btnProfileName.setOnClickListener(view -> showNameWindow());
        btnProfileAbout.setOnClickListener(view -> showAboutWindow());
        btnProfileId.setOnClickListener(view -> copyId());

        showUserData();
    }

    private void userLogOut() {
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
        }
    }

    private void showUserData() {
        fUser = auth.getCurrentUser();
        if(fUser != null){
            name.setText(fUser.getDisplayName());
            email.setText(fUser.getEmail());

            ref.child(key).child("userId").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String idUser = snapshot.getValue().toString();
                    id.setText(idUser);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {}
            });

            ref.child(key).child("photoUrl").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    try {
                        String photoUser = snapshot.getValue().toString();
                        Picasso.get()
                                .load(Uri.parse(photoUser))
                                .placeholder(R.drawable.ic_avatar)
                                .into(photoProfile);
                    }catch (NullPointerException ignored){ }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) { }
            });

            ref.child(key).child("about").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    try {
                        String aboutUser = snapshot.getValue().toString();
                        about.setText(aboutUser);
                    }catch (NullPointerException ignored){ }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) { }
            });

        }
    }

    private void copyId() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", id.getText().toString());
        clipboard.setPrimaryClip(clip);
        if (clip != null) {
            Toast.makeText(ProfileActivity.this, "id скопировано!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showNameWindow() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View loginWindow = inflater.inflate(R.layout.window_edit_name, null);
        dialog.setView(loginWindow);

        MaterialEditText name = loginWindow.findViewById(R.id.btn_edit_name);

        dialog.setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.dismiss());

        dialog.setPositiveButton("Готово", (dialogInterface, i) -> {
            if(TextUtils.isEmpty(name.getText().toString())){
                Snackbar.make(relativeLayout, "Вы не внесли изменения", Snackbar.LENGTH_SHORT).show();
                return;
            }
            ref.child(key).child("name").setValue(name.getText().toString());

            profileUpdate = new UserProfileChangeRequest.Builder()
                    .setDisplayName(name.getText().toString())
                    .build();

            fUser.updateProfile(profileUpdate)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()){
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                            Toast.makeText(ProfileActivity.this, "Изменения сохранены", Toast.LENGTH_SHORT).show();
                        }
                    });
        });
        dialog.show();
    }

    private void showAboutWindow() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View loginWindow = inflater.inflate(R.layout.window_edit_about, null);
        dialog.setView(loginWindow);

        MaterialEditText about = loginWindow.findViewById(R.id.btn_edit_about);

        dialog.setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.dismiss());

        dialog.setPositiveButton("Готово", (dialogInterface, i) -> {
            if(TextUtils.isEmpty(about.getText().toString())){
                Snackbar.make(relativeLayout, "Вы не внесли изменения", Snackbar.LENGTH_SHORT).show();
            }
            ref.child(key).child("about").setValue(about.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                    Toast.makeText(ProfileActivity.this, "Изменения сохранены", Toast.LENGTH_SHORT).show();
                }
            });
        });

        dialog.show();
    }

    private void showMainActivity() {
        startActivity(new Intent(ProfileActivity.this, MainActivity.class));
    }

    private void changePhotoUser() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setRequestedSize(600, 600)
                .setCropShape(CropImageView.CropShape.OVAL)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            resultUri = result.getUri();
            mountainImagesRef = storageRef.child(auth.getCurrentUser().getUid())
                    .child("Image/profile_image");
            mountainImagesRef.putFile(resultUri).addOnSuccessListener(taskSnapshot -> mountainImagesRef.getDownloadUrl()
                    .addOnSuccessListener(uri -> {
                        String photoUrl = uri.toString();

                        ref.child(key).child("photoUrl").setValue(photoUrl);

                        ref.child(key).child("photoUrl").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                String photoUser = snapshot.getValue().toString();
                                Picasso.get()
                                        .load(Uri.parse(photoUser))
                                        .placeholder(R.drawable.ic_avatar)
                                        .into(photoProfile);
                                Toast.makeText(ProfileActivity.this, "Фотография успешно обновлена!", Toast.LENGTH_LONG).show();
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError error) { }
                        });
                    }));
        }

    }
}
